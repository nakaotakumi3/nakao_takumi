<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー編集画面</title>
<link href="./css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="main-contents">
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="errorMessage">
						<li><c:out value="${errorMessage}" />
					</c:forEach>
				</ul>
			</div>
		</c:if>
	</div>

		<a href="manegement">ユーザー管理</a>
		<form action="setting" method="post"><br />

			<label for="account">アカウント名</label> <input name="account"id="account" value="${user.account}" /> <br />

			<label for="password">パスワード</label> <input id="password" /> <br />

			<label for="rePassword">確認パスワード</label> <input name="rePassword" type="password" id="rePassword" /> <br />

			<label for="name">名前</label> <input name="name" id="name" value="${user.name}" /><br />

			<c:if test="${ loginUser.id != user.id }">
				<label for= "branchid">	支社</label>
				<select name="branchid" id="branchid" >
					<c:forEach items="${branches}" var="branch">
						<c:if test="${branch.id == user.branchId }">
							<option value="${branch.id}" selected> ${branch.name}</option>
						</c:if>
						<c:if test="${branch.id != user.branchId }">
							<option value="${branch.id}"> ${branch.name}</option>
						</c:if>
					</c:forEach>
				</select>
				<label for="departmentid"> 部署 </label>
				<select name="departmentid" id="departmentid" >
					<c:forEach items="${departments}" var="department">
						<c:if test="${department.id == user.departmentId }">
							<option value="${department.id}" selected> ${department.name}</option>
						</c:if>
						 <c:if test="${department.id != user.departmentId }">
							<option value="${department.id}"> ${department.name}</option>
						</c:if>
					</c:forEach>
				</select>
			</c:if>


			<!--<c:if test="${ loginUser.id == user.id }">
				<label for= "branchid">	支社</label>
				<select name="branchid" id="branchid" >
					<c:forEach items="${branches}" var="branch">
						<c:if test="${branch.id == user.branchId }">
							 <option value="${branch.id}" selected> ${branch.name}</option>
						</c:if>
						<c:if test="${branch.id != user.branchId }">
							<option value="${branch.id}"> ${branch.name}</option>
						</c:if>
					</c:forEach>
				</select>


			<label for="departmentid"> 部署 </label>
				<select name="departmentid" id="departmentid" >
					<c:forEach items="${departments}" var="department">
						<c:if test="${department.id == user.departmentId }">
							 <option value="${department.id}" selected> ${department.name}</option>
						</c:if>
						 <c:if test="${department.id != user.departmentId }">
							<option value="${department.id}"> ${department.name}</option>
						</c:if>
					</c:forEach>
				</select>
			</c:if>-->
			<input type ="hidden" name ="userId" value="${user.id}" >
			<input type="submit" value="更新" /> <br />
		</form>

</body>
</html>