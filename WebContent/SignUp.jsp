<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>ユーザー登録</title>
	<link href="./css/style.css" rel="stylesheet" type="text/css">
	</head>
	<body>

			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="errorMessage">
							<li><c:out value="${errorMessage}" />
						</c:forEach>
					</ul>
				</div>
			</c:if>

			<form action="signup" method="post"><br />

				<a href="manegement">ユーザー管理</a>

				<label for="account">アカウント名</label> <input name="account"id="account" value="${account}"/> <br />

				<label for="password">パスワード</label> <input id="password" name="password" /> <br />

				<label for="rePassword">確認パスワード</label> <input name="rePassword" type="password" id="rePassword" /> <br />

				<label for="name">名前</label> <input name="name" id="name" value="${name}"/><br />

				<label for= "branchid">	支社</label>
				<select name="branchid" id="branchid" >
					<c:forEach items="${branches}" var="branch">
						<option value="${branch.id}"> ${branch.name}</option>
					</c:forEach>
				</select>

				<label for="departmentid"> 部署 </label>
				<select name="departmentid" id="departmentid" >
					<c:forEach items="${departments}" var="department">
						<option value="${department.id}"> ${department.name}</option>
					</c:forEach>
				</select>

				<br /> <input type="submit" value="登録" /> <br /> <a href="manegement">戻る</a>
			</form>

			<div class="copyright">Copyright(c)Your Name</div>

	</body>
</html>