<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<div class="main-contents">
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="errorMessage">
						<li><c:out value="${errorMessage}" />
					</c:forEach>
				</ul>
			</div>
		</c:if>

		<form action="message" method="post">

			<label for="title">件名</label>
			<input name="title" value="${title}" id="title" /> <br />

			<label for="category">カテゴリー</label>
			 <input name="category" value="${category}" id="category" /> <br />

			<label for="text">投稿内容</label>
			<textarea name="text" cols="35" rows="5" id="text">${text}</textarea><br />

			<input type="submit" value="投稿" /> <br /> <a href="./">戻る</a>
		</form>

		<div class="copyright">Copyright(c)Your Name</div>
	</div>
</body>
</html>