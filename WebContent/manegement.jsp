<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー管理画面</title>
<link href="./css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
<c:if test="${ not empty errorMessages }">
		<div class="errorMessages">
			<ul>
				<c:forEach items="${errorMessages}" var="errorMessage">
					<li><c:out value="${errorMessage}" />
				</c:forEach>
			</ul>
		</div>
		<c:remove var="errorMessages" scope ="session"/>
	</c:if>
	<div class="header">
		<a href="./">ホーム</a>
		<a href="signup">ユーザー新規登録</a>
	</div>

	<c:forEach items="${userbranchdepartments}" var="userbranchdepartment">

			<div class="account">
				<label for="account">アカウント名</label>
					<c:out value="${userbranchdepartment.account}" />
			</div>
			<div class="name">
				<c:out value="${userbranchdepartment.name}" />
			</div>

			<div class="branchName">
				<c:out value="${userbranchdepartment.branchName}" />
			</div>

			<div class="departmentName">
				<c:out value="${userbranchdepartment.departmentName}" />
			</div>

			<div class="isStopped">
				<c:out value="${userbranchdepartment.isStopped}" />
			</div>

			<form action="setting" method="get"><br />
				<input type ="hidden" name = "userId" value="${userbranchdepartment.id}" >
				<input type="submit" value="編集" /> <br />
			</form>


			<c:if test="${ loginUser.id != userbranchdepartment.id }">

			<form action="stop" method="post">
				<input type ="hidden" name = "userId" value="${userbranchdepartment.id}" >
					<c:if test="${userbranchdepartment.isStopped == 0 }">
						<input type ="hidden" name = "isStopped" value="1"  >
						<input type="submit" value="停止" onClick="confirmStop(); return false" />
							<script>
								function confirmStop(){

									if(window.confirm('停止させますか？')){
										comment.check().submit();
										location.href = "./";
										return true;
									}else{
										window.alert('停止はキャンセルされました');
										return false;
									}
								}
							</script>
					</c:if>



				<c:if test="${userbranchdepartment.isStopped == 1 }">
					<input type ="hidden" name = "userId" value="${userbranchdepartment.id}" >
					<input type ="hidden" name = "isStopped" value="0" >
					<input type="submit" value="復活" onClick="confirmRebron(); return false" /> <br />
					<script>
						function confirmRebron(){
							if(window.confirm('復活させますか？')){
								comment.check().submit();
								location.href = "./";
								return true;
							}else{
								window.alert('復活はキャンセルされました');
								return false;
							}
						}
					</script>
				</c:if>

			</form>
			</c:if>

	</c:forEach>

	<div class="copyright">Copyright(c)nakao_takumi</div>

</body>
</html>