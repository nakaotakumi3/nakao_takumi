<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ホーム画面</title>
<link href="./css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
<c:if test="${ not empty errorMessages }">
		<div class="errorMessages">
			<ul>
				<c:forEach items="${errorMessages}" var="errorMessage">
					<li><c:out value="${errorMessage}" />
				</c:forEach>
			</ul>
		</div>
		<c:remove var="errorMessages" scope ="session"/>
	</c:if>

	<form action="index.jsp">
	<label for="date">日付</label>
	<input type="date" id="start" name="start" value="${start}">
	～<input type="date" id="end" name="end" value="${end}">

	<label for="">カテゴリ検索</label>
	<input name="category" id="category"  value="${category}">
	<input type="submit" value="絞り込み">
	</form>

	<div class="main-contents">
		<div class="header">

			<form action="home" method="post">
				<a href="message">新規投稿</a>
				 <a href="manegement">ユーザー管理</a>
				 <a href="login">ログアウト</a>
			</form>
		</div>

		<c:forEach items="${messages}" var="message">
			<div class="messages">

				<div class="name">
						<c:out value="${message.name}" />
				</div>

				<div class="title">
					<c:out value="${message.title}" />
				</div>

				<div class="category">
					<c:out value="${message.category}" />
				</div>

				<div class="text">
					<pre><c:out value="${message.text}" /></pre>
				</div>

				<div class="date">
						<fmt:formatDate value="${message.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" />
				</div>

				<c:if test="${loginUser.id == message.userId }">
					<form action="deletemessage" method="post">
						<input type ="hidden" name = "messageId" value="${message.id}" >
						<input type="submit" value="削除" onClick="confirmMessage(); return false" />
						<script>
							function confirmMessage(){

							if(window.confirm('ほんとの本当にいいんですね？')){
								messaGe.check().submit();
								location.href = "./";
										return true;
							}else{

									window.alert('キャンセルされました');
								return false;
								}
							}
						</script>
					</form>
				</c:if>

				<c:forEach items="${comments}" var="comment">

					<c:if test= "${comment.messageId == message.id}" >
						<div class="name">
								<c:out value="${comment.name}" />
						</div>

						<div class="text">
							<pre><c:out value="${comment.text}" /></pre>
						</div>

						<div class="date">
								<fmt:formatDate value="${comment.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" />
						</div>
							<c:if test="${loginUser.id == comment.userId }">
								<form action="deletecomment" method="post">
									<input type ="hidden" name = "commentId" value="${comment.id}" >
									<input type="submit" value="削除" onClick="confirmComment(); return false" />
										<script>
										function confirmComment(){

											if(window.confirm('ほんとの本当にいいんですね？')){
												comment.check().submit();
												location.href = "./";
												return true;
											}else{

												window.alert('キャンセルされました');
												return false;
											}
										}
										</script>
								</form>
						</c:if>
					</c:if>
				</c:forEach>
			</div>

			<label for="text">コメント投稿</label>

				<form action="comment" method="post"><br />

					<textarea name="comment.text" cols="20" rows="5" id="comment.text">${comment.text}</textarea>

					<input type ="hidden" name = "messageId" value="${message.id}" >  <br>

					<input type="submit" value="投稿" /> <br />
				</form>

		</c:forEach>

	</div>
	<div class="copyright">Copyright(c)nakaotakumi</div>

</body>
</html>