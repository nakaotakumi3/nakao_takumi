<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ログイン</title>
<link href="./css/style.css" rel="stylesheet" type="text/css">
</head>
<body>

	<c:if test="${ not empty errorMessages }">
		<div class="errorMessages">
			<ul>
				<c:forEach items="${errorMessages}" var="errorMessage">
					<li><c:out value="${errorMessage}" />
				</c:forEach>
			</ul>
		</div>
		<c:remove var="errorMessages" scope ="session"/>
	</c:if>
	<form action="login" method="post"><br />
		 <label for="account">アカウント名</label> <input name="account" value="${account}" id="account" />  <br />

		 <label for="password">パスワード</label><input name="password" type="password" id="password" /> <br />

		 <input type="submit" value="ログイン" /> <br />
	</form>
	<div class="copyright">Copyright(c)nakao takumi</div>
</body>
</html>