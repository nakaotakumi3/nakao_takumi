package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Department;
import beans.User;
import service.BranchService;
import service.DepartmentService;
import service.UserService;

@WebServlet("/setting")
public class SettingServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();

		List<Department> department = new DepartmentService().select();
		List<Branch> branch = new BranchService().select();
		List<String> errorMessages = new ArrayList<String>();

		String userId = request.getParameter("userId");

		if (!userId.matches("^[0-9]+$")){
			errorMessages.add("不正なパラメーターが入力されました");
			session.setAttribute("errorMessages", errorMessages);
			response.sendRedirect("manegement");
			return;
		}
		User user = new UserService().select(Integer.parseInt(request.getParameter("userId")));

		if( user == null) {
			errorMessages.add("不正なパラメーターが入力されました");
			session.setAttribute("errorMessages", errorMessages);
			response.sendRedirect("manegement");
			return;
		}

		if(StringUtils.isBlank(userId)) {
			errorMessages.add("不正なパラメーターが入力されました");
			session.setAttribute("errorMessages", errorMessages);
			response.sendRedirect("manegement");
			return;
		}

		User loginUser = (User) session.getAttribute("loginUser");

		request.setAttribute("departments",department);
		request.setAttribute("branches",branch);
		request.setAttribute("user", user);
		request.setAttribute("loginUser", loginUser);
		request.getRequestDispatcher("setting.jsp").forward(request, response);
	}
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		List<Department> department = new DepartmentService().select();
		List<Branch> branch = new BranchService().select();
		List<String> errorMessages = new ArrayList<String>();

		User user = getUser(request);

		if (!isValid(user, errorMessages)) {
			request.setAttribute("errorMessages", errorMessages);
			request.setAttribute("user", user);
			request.setAttribute("branches", branch);
			request.setAttribute("departments", department);
			request.getRequestDispatcher("setting.jsp").forward(request, response);
			return;
		}

		new UserService().update(user);
		response.sendRedirect("manegement");
	}

	private User getUser(HttpServletRequest request) throws IOException, ServletException {

		User user = new User();
		user.setId(Integer.parseInt(request.getParameter("userId")));
		user.setAccount(request.getParameter("account"));
		user.setPassword(request.getParameter("password"));
		user.setRepassword(request.getParameter("rePassword"));

		user.setName(request.getParameter("name"));
		user.setBranchId(Integer.parseInt(request.getParameter("branchid")));
		user.setDepartmentId(Integer.parseInt(request.getParameter("departmentid")));

		return user;
	}
	private boolean isValid(User user, List<String> errorMessages) {

		String name = user.getName();
		String account = user.getAccount();
		String password = user.getPassword();
		String rePassword = user.getRepassword();
		User muraosa = new UserService().select(user.getAccount());
		User muraosaId = new UserService().select(user.getId());
		String username = muraosaId.getAccount();
		int branch = user.getBranchId();
		int department = user.getDepartmentId();

		if (StringUtils.isBlank(name)) {
			errorMessages.add("名前を入力してください");
		} else if (10 < name.length()) {
			errorMessages.add("名前を10文字以下で入力してください");
		}

		if (StringUtils.isBlank(account)) {
			errorMessages.add("アカウント名を入力してください");

		} else if ( !account.matches( "^[0-9a-zA-Z]+$"))  {
				errorMessages.add("半角英数字で記入してください");

		} else if (!(6 <= account.length() && 20 >= account.length())) {
			errorMessages.add("アカウント名は6文字以上20文字以下で入力してください");
		} else if(( muraosa != null) && (!account.equals(username))) {
			errorMessages.add("アカウントが重複しています");
		}


		if (StringUtils.isBlank(password)) {
			errorMessages.add("パスワードを入力してください");

		} else if (!password.matches("^[-_@+*;:#$%&A-Za-z0-9]+$")) {
			errorMessages.add("パスワードを記号を含むすべての半角英数字で記入してください");

		} else if (!(6 < password.length() && 20 > password.length())) {
			errorMessages.add("パスワード名は6文字以上20文字以下で入力してください");
		}

		if(StringUtils.isEmpty(rePassword)) {
			errorMessages.add("確認用パスワードを入力してください");

		}else if (!password.equals(rePassword)) {
			errorMessages.add("パスワードと確認用パスワードを一致させてください");
		}


		if (branch == 3 && (department == 1 || department == 2)) {
			errorMessages.add("支社と部署の組み合わせが間違っています");

		} else if (branch == 1 && (department == 3 || department == 4)) {
			errorMessages.add("支社と部署の組み合わせが間違っています");

		} else if (branch == 4 && (department == 1 || department == 2)) {
			errorMessages.add("支社と部署の組み合わせが間違っています");


		} else if (branch == 2 && (department == 3 || department == 4)) {
			errorMessages.add("支社と部署の組み合わせが間違っています");
		}

		if (errorMessages.size() != 0) {
			return false;
		}
		return true;
	}
}
