package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Message;
import beans.User;
import service.MessageService;

@WebServlet("/message")
public class MessageServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		request.getRequestDispatcher("message.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		HttpSession session = request.getSession();

		String title = request.getParameter("title");
		String category = request.getParameter("category");
		String text = request.getParameter("text");

		

		List<String> errorMessages = new ArrayList<String>();
		if (!isValid(request, errorMessages)) {
			request.setAttribute("errorMessages", errorMessages);
			request.setAttribute("text", text);
			request.setAttribute("category", category);
			request.setAttribute("title", title);
			request.getRequestDispatcher("message.jsp").forward(request, response);
			return;
		}

		Message message = new Message();

		message.setTitle(title);
		message.setCategory(category);
		message.setText(text);

		User user = (User) session.getAttribute("loginUser");
		message.setUserId(user.getId());

		new MessageService().insert(message);
		response.sendRedirect("./");
	}

	private boolean isValid(HttpServletRequest request, List<String> errorMessages) {

		String title = request.getParameter("title");
		String category = request.getParameter("category");
		String text = request.getParameter("text");

		if (StringUtils.isBlank(title)) {
			errorMessages.add("件名を入力してください");
		} else if (30 < title.length()) {
			errorMessages.add("30文字以下で入力してください");
		}

		if (StringUtils.isBlank(category)) {
			errorMessages.add("カテゴリーを入力してください");
		} else if (10 < category.length()) {
			errorMessages.add("10文字以下で入力してください");
		}

		if (StringUtils.isBlank(text)) {
			errorMessages.add("メッセージを入力してください");
		} else if (1000 < text.length()) {
			errorMessages.add("1000文字以下で入力してください");
		}

		if (errorMessages.size() != 0) {
			return false;
		}
		return true;
	}
}
