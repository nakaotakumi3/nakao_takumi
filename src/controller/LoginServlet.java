package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.User;
import service.UserService;


@WebServlet(urlPatterns = {"/login"})
public class LoginServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		request.getRequestDispatcher("login.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		String account = request.getParameter("account");
		String password = request.getParameter("password");
		List<String> errorMessages = new ArrayList<String>();

		User user = new UserService().select(account, password);

		if (!isValid(request, errorMessages)) {
			request.setAttribute("errorMessages", errorMessages);
			request.setAttribute("account", account);
			request.setAttribute("password", password);
			request.getRequestDispatcher("login.jsp").forward(request, response);
			return;


		}
		if (user == null) {
			errorMessages.add("アカウントかパスワードが間違ってます");
			request.setAttribute("account", account);
			request.setAttribute("errorMessages", errorMessages);
			request.getRequestDispatcher("login.jsp").forward(request, response);
			return;
		}

		if (user.getIsStopped() == 1) {
			errorMessages.add("アカウントまたはパスワードが誤っています");
			request.setAttribute("errorMessages",errorMessages);
			request.getRequestDispatcher("login.jsp").forward(request, response);
			return;
		}

		HttpSession session = request.getSession();
		session.setAttribute("loginUser", user);
		response.sendRedirect("./");
	}
	private boolean isValid(HttpServletRequest request, List<String> errorMessages) {

		String account = request.getParameter("account");
		String password = request.getParameter("password");


		if (StringUtils.isBlank(account)) {
			errorMessages.add("アカウントを入力してください");

		}
		if (StringUtils.isBlank(password)) {
			errorMessages.add("パスワードを入力してください");

		}
		if (errorMessages.size() != 0) {
			return false;
		}
		return true;
	}
}