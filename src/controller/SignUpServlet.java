package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Department;
import beans.User;
import service.BranchService;
import service.DepartmentService;
import service.UserService;


@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		List<Department> department = new DepartmentService().select();
		List<Branch> branch = new BranchService().select();

		request.setAttribute("departments", department);
		request.setAttribute("branches", branch);

		request.getRequestDispatcher("signup.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		List<Department> department = new DepartmentService().select();
		List<Branch> branch = new BranchService().select();
		List<String> errorMessages = new ArrayList<String>();
		String account = request.getParameter("account");
		String password = request.getParameter("password");
		String rePassword = request.getParameter("rePassword");
		String name = request.getParameter("name");

		User user = getUser(request);
		if (!isValid(user,request, errorMessages)) {
			request.setAttribute("account",account);
			request.setAttribute("password",password);
			request.setAttribute("rePassword",rePassword);
			request.setAttribute("name",name);
			request.setAttribute("departments", department);
			request.setAttribute("branches", branch);
			request.setAttribute("errorMessages", errorMessages);
			request.getRequestDispatcher("signup.jsp").forward(request, response);
			return;
		}
		new UserService().insert(user);
		response.sendRedirect("manegement");
	}

	private User getUser(HttpServletRequest request) throws IOException, ServletException {

		User user = new User();
		user.setAccount(request.getParameter("account"));
		user.setPassword(request.getParameter("password"));
		user.setRepassword(request.getParameter("rePassword"));
		user.setName(request.getParameter("name"));
		user.setBranchId(Integer.parseInt(request.getParameter("branchid")));
		user.setDepartmentId(Integer.parseInt(request.getParameter("departmentid")));

		return user;
	}
	private boolean isValid(User user,HttpServletRequest request, List<String> errorMessages) {

		String name = user.getName();
		String account = user.getAccount();
		String password = user.getPassword();
		String rePassword = user.getRepassword();
		int branch = user.getBranchId();
		int department = user.getDepartmentId();
		User murabito  = new UserService().select(user.getAccount());

		if (StringUtils.isEmpty(name) && (10 < name.length())) {
			errorMessages.add("名前は10文字以下で入力してください");
		}
		if (StringUtils.isEmpty(account)) {
			errorMessages.add("アカウント名を入力してください");

		} else if (account.matches("^[a-zA-Z0-9 -/:-@\\[-\\`\\{-\\~]+$\r\n" +"")) {
			errorMessages.add("半角英数字で記入してください");

		} else if (!(6 <= account.length() && 20 >= account.length())) {
			errorMessages.add("アカウント名は6文字以上20文字以下で入力してください");
		} else if ( murabito != null) {
			errorMessages.add("アカウントが重複しています");
		}

		if (StringUtils.isEmpty(password)) {
			errorMessages.add("パスワードを入力してください");

		} else if (password.matches("^[a-zA-Z0-9 -/:-@\\[-\\`\\{-\\~]+$\r\n" +"")) {

			errorMessages.add("パスワードを記号を含むすべての半角英数字で記入してください");

		} else if (6 > password.length() || 20 < password.length()) {
			errorMessages.add("パスワード名は6文字以上20文字以下で入力してください");
		}
		if(StringUtils.isEmpty(rePassword)) {
			errorMessages.add("確認用パスワードを入力してください");

		}else if (!password.equals(rePassword)) {
			errorMessages.add("パスワードと確認用パスワードを一致させてください");
		}


		if (!(((branch == 1) && ((department == 1) || (department == 2)))	|| (((branch != 1) && ((department == 3) || (department == 4))))))  {
			errorMessages.add("支店と部署の組み合わせが正しくありません");
		}


		if (errorMessages.size() != 0) {
			return false;
		}
		return true;
	}
}