package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Comment;
import beans.User;
import service.CommentService;


@WebServlet("/comment")
public class CommentServlet extends HttpServlet {


	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		HttpSession session = request.getSession();
		List<String> errorMessages = new ArrayList<String>();

		String text = request.getParameter("comment.text");


		if (!isValid(request, errorMessages)) {
			session.setAttribute("errorMessages", errorMessages);
			session.setAttribute("text",text);
			response.sendRedirect("./");
			return;

		}

		int message_id = Integer.parseInt(request.getParameter("messageId"));

		Comment comment = new Comment();
		User user = (User) session.getAttribute("loginUser");

		comment.setText(text);
		comment.setMessageId(message_id);

		comment.setUserId(user.getId());

		new CommentService().insert(comment);
		response.sendRedirect("./");
	}

		private boolean isValid(HttpServletRequest request, List<String> errorMessages) {

		String text = request.getParameter("comment.text");

		if (StringUtils.isBlank(text)) {
			errorMessages.add("コメント投稿を入力してください");

		} else if (500 < text.length()) {

			errorMessages.add("500文字以下で入力してください");
		}
		if (errorMessages.size() != 0) {
			return false;
		}
		return true;
	}

}
