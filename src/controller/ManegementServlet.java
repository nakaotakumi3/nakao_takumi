package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import beans.UserBranchDepartment;
import service.UserService;

@WebServlet( urlPatterns = {"/manegement"})
public class ManegementServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		HttpSession session = request.getSession();

		List<UserBranchDepartment> user = new UserService().select();

		User loginUser = (User) session.getAttribute("loginUser");

		request.setAttribute("loginUser",loginUser);

		request.setAttribute("userbranchdepartments", user);
		request.getRequestDispatcher("/manegement.jsp").forward(request, response);
	}
}
