package filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;


@WebFilter({"/signup","/manegement","/setting"})
public class ManegementFilter implements Filter {

	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		HttpSession session = ((HttpServletRequest)request).getSession();


		List<String> errorMessages = new ArrayList<String>();

		User loginUser = (User) session.getAttribute("loginUser");

		int branchId = loginUser.getBranchId();

		int departmentId = loginUser.getDepartmentId();


			if ((branchId != 1) && (departmentId !=1)){
				errorMessages.add("アクセス権限がありません");
				session.setAttribute("errorMessages",errorMessages);
				((HttpServletResponse) response).sendRedirect("./");
				return;
			}


		chain.doFilter(request, response);
	}

	public void init(FilterConfig fConfig) throws ServletException {

	}
	public ManegementFilter() {
	}

	public void destroy() {
	}

}
