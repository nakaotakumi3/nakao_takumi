package service;

import static util.CloseableUtil.*;
import static util.DButils.*;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.Message;
import beans.UserMessage;
import dao.MessageDao;
import dao.UserMessageDao;

public class MessageService {

	public void insert(Message message) {

		Connection connection = null;
		try {
			connection = getConnection();
			new MessageDao().insert(connection, message);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	public List<UserMessage> select(String start,String end,String category) {
		final int LIMIT_NUM = 1000;

		 Date date = new Date();
		Connection connection = null;
		try {
			connection = getConnection();

			if(!StringUtils.isEmpty(start)) {

				start = start + " 00:00:00";
			} else {
				start = "2020-01-01 00:00:00";
			}

			if(!StringUtils.isEmpty(end)) {
				end = end + " 23:59:59";
			} else {
				String sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(date);
				end = sdf;
			}

			if(!StringUtils.isEmpty(category)) {
				category = "%" + category + "%";
			}else {
				category = null;
			}


			List<UserMessage> messages = new UserMessageDao().select(connection,LIMIT_NUM,start,end,category);
			commit(connection);
			return messages;

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	public void delete(int messageId) {

		Connection connection = null;
		try {
			connection = getConnection();

			new MessageDao().delete(connection, messageId);
			commit(connection);
			return;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}
