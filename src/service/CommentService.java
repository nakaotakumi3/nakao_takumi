package service;

import static util.CloseableUtil.*;
import static util.DButils.*;

import java.sql.Connection;
import java.util.List;

import beans.Comment;
import beans.UserComment;
import dao.CommentDao;
import dao.UserCommentDao;


public class CommentService {

	public void insert(Comment comment) {

		Connection connection = null;
		try {
			connection = getConnection();
			new CommentDao().insert(connection, comment);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public List<UserComment> select() {
		final int LIMIT_NUM = 1000;

		Connection connection = null;
		try {
			connection = getConnection();

			commit(connection);
			List<UserComment> comments = new UserCommentDao().select(connection,  LIMIT_NUM);
			return comments;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void delete(int commentId) {

		Connection connection = null;
		try {
			connection = getConnection();

			new CommentDao().delete(connection, commentId);
			commit(connection);
			return;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}
