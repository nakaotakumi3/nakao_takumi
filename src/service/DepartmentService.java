package service;

import static util.CloseableUtil.*;
import static util.DButils.*;

import java.sql.Connection;
import java.util.List;

import beans.Department;
import dao.DepartmentDao;

public class DepartmentService {
	public List<Department> select() {

		Connection connection = null;
		try {
			connection = getConnection();

			commit(connection);
			List<Department> departments = new DepartmentDao().select(connection);
			return departments;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}
