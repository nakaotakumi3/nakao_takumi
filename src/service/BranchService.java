package service;

import static util.CloseableUtil.*;
import static util.DButils.*;

import java.sql.Connection;
import java.util.List;

import beans.Branch;
import dao.BranchDao;

public class BranchService {
	public List<Branch> select() {

		Connection connection = null;
		try {
			connection = getConnection();

			commit(connection);
			List<Branch> branches = new BranchDao().select(connection);
			return branches;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}
