package dao;

import static util.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import beans.Message;
import exception.SQLRuntimeException;


public class MessageDao {
	public void insert(Connection connection, Message message) {
		PreparedStatement ps = null;

		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO messages ( ");
			sql.append("    title, ");
			sql.append("    category, ");
			sql.append("    text, ");
			sql.append("    user_id, ");
			sql.append("    created_date, ");
			sql.append("    updated_date ");
			sql.append(") VALUES ( ");
			sql.append("    ?, ");                  //title
			sql.append("    ?, ");                  // category
			sql.append("    ?, ");                  // text
			sql.append("    ?, ");                  // user_id
			sql.append("    CURRENT_TIMESTAMP, ");  // created_date
			sql.append("    CURRENT_TIMESTAMP ");   // updated_date
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, message.getTitle());
			ps.setString(2, message.getCategory());
			ps.setString(3, message.getText());
			ps.setInt(4, message.getUserId());

			ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	public void delete(Connection connection, int messageId) {

		PreparedStatement ps = null;
		try {
	
			String sql = "DELETE FROM messages WHERE id = ?" ;

			ps = connection.prepareStatement(sql);

			ps.setInt(1, messageId);

			ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

}
