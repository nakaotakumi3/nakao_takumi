package dao;

import static util.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.UserBranchDepartment;
import exception.SQLRuntimeException;

public class UserBranchDepartmentDao {
	public List<UserBranchDepartment> select(Connection connection, int limit){
		PreparedStatement ps = null;
		try {

			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("     users.id as id, ");
			sql.append("     users.account as account, ");
			sql.append("     users.name as name, ");
			sql.append("     users.is_stopped as isStopped, ");
			sql.append("     branches.name as branchName, ");
			sql.append("     departments.name as departmentName ");
			sql.append("FROM users ");
			sql.append("INNER JOIN branches ");
			sql.append("ON users.branch_id = branches.id ");
			sql.append("INNER JOIN departments ");
			sql.append("ON users.department_id = departments.id ");
			sql.append("ORDER BY users.created_date DESC limit " + limit );

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();

			List<UserBranchDepartment> userbranchdepartment = toUserBranchDepartment(rs);

			return  userbranchdepartment;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserBranchDepartment> toUserBranchDepartment(ResultSet rs) throws SQLException {

		List<UserBranchDepartment>  userbranchdepartment = new ArrayList<UserBranchDepartment>();
		try {
			while (rs.next()) {
				UserBranchDepartment user = new UserBranchDepartment();

				user.setId(rs.getInt("id"));
				user.setAccount(rs.getString("account"));
				user.setName(rs.getString("name"));
				user.setIsStopped(rs.getInt("isStopped"));
				user.setBranchName(rs.getString("branchName"));
				user.setDepartmentName(rs.getString("DepartmentName"));

				userbranchdepartment.add(user);
			}
			return userbranchdepartment;
		} finally {
			close(rs);
		}
	}
}
