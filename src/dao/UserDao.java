package dao;

import static util.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.mysql.jdbc.StringUtils;

import beans.User;
import exception.SQLRuntimeException;

public class UserDao {

	public void insert(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO users ( ");
			sql.append("    id," );
			sql.append("    account, ");
			sql.append("    password, ");
			sql.append("    name, ");
			sql.append("    branch_id, ");
			sql.append("    department_id, ");
			sql.append("    is_stopped, ");
			sql.append("    created_date, ");
			sql.append("    updated_date ");
			sql.append(") VALUES ( ");
			sql.append("    ?, ");              // id
			sql.append("    ?, ");              // account
			sql.append("    ?, ");              // password
			sql.append("    ?, ");              // name
			sql.append("    ?, ");              // branch_id
			sql.append("    ?, ");              // department_id
			sql.append("    ?, ");              // is_stopped
			sql.append("    CURRENT_TIMESTAMP, ");  // created_date
			sql.append("    CURRENT_TIMESTAMP ");       // updated_date
			sql.append(")");

			ps = ((Connection) connection).prepareStatement(sql.toString());

			ps.setInt(1, user.getId());
			ps.setString(2, user.getAccount());
			ps.setString(3, user.getPassword());
			ps.setString(4, user.getName());
			ps.setInt(5, user.getBranchId());
			ps.setInt(6, user.getDepartmentId());
			ps.setInt(7, user.getIsStopped());


			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	public User select(Connection connection, String account, String password) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE account = ? AND password = ?";

			ps = connection.prepareStatement(sql);

			ps.setString(1,  account);
			ps.setString(2, password);

			ResultSet rs = ps.executeQuery();

			List<User> users = toUsers(rs);
			if (users.isEmpty()) {
				return null;
			} else if (2 <= users.size()) {
				throw new IllegalStateException("ユーザーが重複しています");
			} else {
				return users.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<User> toUsers(ResultSet rs) throws SQLException {

		List<User> users = new ArrayList<User>();
		try {
			while (rs.next()) {
				User user = new User();
				user.setId(rs.getInt("id"));
				user.setAccount(rs.getString("account"));
				user.setPassword(rs.getString("password"));
				user.setName(rs.getString("name"));
				user.setBranchId(rs.getInt("branch_id"));
				user.setDepartmentId(rs.getInt("department_id"));
				user.setIsStopped(rs.getInt("is_stopped"));

				user.setCreatedDate(rs.getTimestamp("created_date"));
				user.setUpdatedDate(rs.getTimestamp("updated_date"));

				users.add(user);
			}
			return users;
		} finally {
			close(rs);
		}
	}
	public User select(Connection connection, int userId){
		PreparedStatement ps = null;

		try {
			String sql = "SELECT * FROM users WHERE id = ? ";

			ps = connection.prepareStatement(sql);
			ps.setInt(1, userId);

			ResultSet rs = ps.executeQuery();
			List<User> user = toUsers(rs);
			if(user.isEmpty()) {
				return null;
			}
			return user.get(0);

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	public void update(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET ");
			sql.append("    account = ?, ");
			sql.append("    name = ?, ");
			if( !StringUtils.isNullOrEmpty(user.getPassword())) {
				sql.append("    password = ?, ");
			}
			sql.append("    branch_id= ?, ");
			sql.append("    department_id = ?, ");
			sql.append("    updated_date = CURRENT_TIMESTAMP ");
			sql.append("WHERE id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getAccount());
			ps.setString(2, user.getName());
			if( !StringUtils.isNullOrEmpty(user.getPassword())) {
				ps.setString(3, user.getPassword());
				ps.setInt(4, user.getBranchId());
				ps.setInt(5, user.getDepartmentId());
				ps.setInt(6, user.getId());
			}
			ps.setInt(3, user.getBranchId());
			ps.setInt(4, user.getDepartmentId());
			ps.setInt(5, user.getId());



			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	public User select(Connection connection, String account) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE account = ?";

			ps = connection.prepareStatement(sql);
			ps.setString(1, account);

			ResultSet rs = ps.executeQuery();

			List<User> users = toUsers(rs);
			if (users.isEmpty()) {
				return null;
			} else if (2 <= users.size()) {
				throw new IllegalStateException("ユーザーが重複しています");
			} else {
				return users.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public void Stopped(Connection connection, User user){
		PreparedStatement ps = null;
		try {

			StringBuilder sql = new StringBuilder();
			sql.append(" UPDATE users SET");
			sql.append(" is_stopped = ? " );
			sql.append(" WHERE id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, user.getIsStopped());
			ps.setInt(2, user.getId());

			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);

		}
	}

}

